"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic import TemplateView
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Urban Connection API Documentation')

urlpatterns = [
    re_path('', include('users.urls')),
    re_path('', include('campaigns.urls')),
    re_path('', include('banners.urls')),
    path('elastic_rtblogs/', include('elastic_rtblogs.urls')),
    path('polls/', include('polls.urls')),
    path('admin/', admin.site.urls),
    path('swagger/', schema_view),
]