from datetime import datetime

from elasticsearch_dsl import analyzer

from django_elasticsearch_dsl import DocType, Index, fields 

from elastic_rtblogs import models as rtblogs_models


rtblogs_index = Index(f'rtblogs-{datetime.now().strftime("%Y.%m.%d")}')
rtblogs_index.settings(
    number_of_shards=1,
    number_of_replicas=0
)

html_strip = analyzer(
    'html_strip',
    tokenizer="standard",
    filter=["standard", "lowercase"], #, "stop", "snowball"],
    char_filter=["html_strip"]
)

@rtblogs_index.doc_type
class RtblogsDocument(DocType):
    """rtblogs-2019.09.18 elasticsearch document"""
    timestamp = fields.DateField()
    version = fields.StringField()
    consumer_group = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword'),
        }
    )
    field = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword'),
        }
    )
    index = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword'),
        }
    )
    message = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword'),
        }
    )
    serialClass = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword'),
        }
    )
    sev = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword'),
        }
    )
    source = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword'),
        }
    )
    topic = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword'),
        }
    )
    type = fields.StringField(
        analyzer=html_strip,
        fields={
            'raw': fields.StringField(analyzer='keyword'),
        }
    )


    class Meta:
        model = rtblogs_models.Rtblogs 