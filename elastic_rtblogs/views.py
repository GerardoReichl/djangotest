# from django.shortcuts import render
# from rest_framework import generics
# from .models import Banners
# from .serializers import BannersSerializer

# # Create your views here.
# class ListBannersView(generics.ListAPIView):
#     """
#     Provides a get method handler.
#     """
#     queryset = Banners.objects.all()
#     serializer_class = BannersSerializer

from django_elasticsearch_dsl_drf.constants import (
    LOOKUP_FILTER_RANGE,
    LOOKUP_QUERY_IN,
    LOOKUP_QUERY_GT,
    LOOKUP_QUERY_GTE,
    LOOKUP_QUERY_LT,
    LOOKUP_QUERY_LTE,
)
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
    SearchFilterBackend,
)
from django_elasticsearch_dsl_drf.viewsets import DocumentViewSet
 
from . import documents as elastic_rbtlogs_documents
from . import serializers as elastic_rbtlogs_serializers

class RtblogsViewSet(DocumentViewSet):
    document = elastic_rbtlogs_documents.RtblogsDocument
    serializer_class = elastic_rbtlogs_serializers.RtblogsDocumentSerializer
 
    lookup_field = 'index'
    filter_backends = [
        FilteringFilterBackend,
        OrderingFilterBackend,
        DefaultOrderingFilterBackend,
        SearchFilterBackend,
    ]
 
    # Define search fields
    search_fields = (
        'consumer_group',
        'source',
    )
 
    # Filter fields
    filter_fields = {
  #       'id': {
  #           'field': 'id',
  #           'lookups': [
  #               LOOKUP_FILTER_RANGE,
  #               LOOKUP_QUERY_IN,
  #               LOOKUP_QUERY_GT,
  #               LOOKUP_QUERY_GTE,
  #               LOOKUP_QUERY_LT,
  #               LOOKUP_QUERY_LTE,
  #           ],
  #       },
		# 'campaign_id': {
  #           'field': 'campaign_id',
  #           'lookups': [
  #               LOOKUP_FILTER_RANGE,
  #               LOOKUP_QUERY_IN,
  #               LOOKUP_QUERY_GT,
  #               LOOKUP_QUERY_GTE,
  #               LOOKUP_QUERY_LT,
  #               LOOKUP_QUERY_LTE,
  #           ],
  #       },
  #       'name': 'name.raw'
    }
 
    # Define ordering fields
    ordering_fields = {
        # 'id': 'id',
        # 'campaign_id': 'campaign_id',
        # 'name': 'name.raw',
    }

    # Specify default ordering
    ordering = () #('id', 'campaign_id',)  
