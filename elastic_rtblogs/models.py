# -*- coding: UTF-8 -*-
from django.db import models
from datetime import datetime

# Create your models here.
class Rtblogs(models.Model):
    timestamp = models.DateTimeField(db_column='@timestamp')
    version = models.CharField(max_length=255, blank=True, null=True, db_column='@version')
    consumer_group = models.CharField(max_length=255, blank=True, null=True)
    field = models.CharField(max_length=255, blank=True, null=True)
    index = models.CharField(max_length=255, blank=True, null=True)
    instance = models.CharField(max_length=255, blank=True, null=True)
    message = models.CharField(max_length=255, blank=True, null=True)
    serialClass = models.CharField(max_length=255, blank=True, null=True)
    sev = models.CharField(max_length=255, blank=True, null=True)
    source = models.CharField(max_length=255, blank=True, null=True)
    topic = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = f'rtblogs-{datetime.now().strftime("%Y.%m.%d")}'
