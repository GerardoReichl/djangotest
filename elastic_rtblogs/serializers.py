# from rest_framework import serializers
# from .models import Banners


# class BannersSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Banners
#         fields = ("name", "campaign_id", "wins", "hourly_cost", "total_cost")

from django_elasticsearch_dsl_drf.serializers import DocumentSerializer
from elastic_rtblogs import documents as rtblogs_documents
class RtblogsDocumentSerializer(DocumentSerializer):
    class Meta:
        document = rtblogs_documents.RtblogsDocument
        fields = (
            '@timestamp',
            '@version',
            'consumer_group',
            'field',
            'index',
            'instance',
            'message',
            'serialClass',
            'sev',
            'source',
            'topic',
            'type')
