from django.apps import AppConfig
from datetime import datetime

class RtbLogsConfig(AppConfig):

    name = f'rtblogs-{datetime.now().strftime("%Y.%m.%d")}'
