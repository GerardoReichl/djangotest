from django.test import TestCase

# Create your tests here.
from datetime import datetime
from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import Rtblogs
from .serializers import RtblogsDocumentSerializer

class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_entry(timestamp=datetime.now(), version='1.0',
    				 field='test_field', index='test_index', instance='test_instance',
                     message='test_message_hello_world_lorem_ipsum', serialClass='test_serial_class',
    				 sev='test_sev', source='test_source', topic='test_topic', type='test_type'):
        if message != "" and source != "":
            Rtblogs.objects.create(name=name, total_cost=email)

    def setUp(self):

        # Add test data
        self.create_entry(message='test_meesage_hello_world_lorem_ipsum_1', total_cost=100)
        self.create_entry(message='test_meesage_hello_world_lorem_ipsum_2', total_cost=200)
        self.create_entry(message='test_meesage_hello_world_lorem_ipsum_3', total_cost=300)
        self.create_entry(message='test_meesage_hello_world_lorem_ipsum_4', total_cost=400)


class GetAllRtblogsTest(BaseViewTest):

    def test_get_all_rtblogs(self):
        """
        This test ensures that all banners added in the setUp method
        exist when we make a GET request to the banners/ endpoint
        """
        # Hit the API endpoint
        response = self.client.get(
            reverse(f'rtblogs-{datetime.now().strftime('%Y.%m.%d')}', kwargs={'version': 'v1'})
        )
        # Fetch the data from db
        expected = Rtblogs.objects.all()
        serialized = RtblogsDocumentSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


        