"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.urls import path
# from .views import ListBannersView


# urlpatterns = [
#     path('banners/', ListBannersView.as_view(), name="Banners-all")
# ]

from rest_framework.routers import SimpleRouter

from elastic_rtblogs import views
 

app_name = 'elastic_rtblogs' 

router = SimpleRouter()
router.register(
    prefix=r'',
    base_name='elastic_rtblogs',
    viewset=views.RtblogsViewSet
)
urlpatterns = router.urls
