import requests
import json
from datetime import datetime
from pprint import pprint


# Host and index are global parametters.
ELASTIC_HOST = 'http://elastic1:9200/'

ELASTIC_KIBANA_INDEX = f'.kibana'
ELASTIC_REQUESTS_INDEX = f'requests-{datetime.now().strftime("%Y.%m.%d")}'
ELASTIC_LOG_INDEX = f'rtblogs-{datetime.now().strftime("%Y.%m.%d")}'
ELASTIC_STATS_INDEX = f'stats-{datetime.now().strftime("%Y.%m.%d")}'
DEFAULT_TABLES_LIST = [ELASTIC_KIBANA_INDEX, ELASTIC_REQUESTS_INDEX,
                       ELASTIC_LOG_INDEX, ELASTIC_STATS_INDEX]

# Parametters for table showing.
ELASTIC_FORMAT_TXT = '_xpack/sql?format=txt'
ELASTIC_JSON_TABLES_QUERY = json.loads('{"query":"SHOW TABLES"}')


def show_index_properties(verbose=False):
    result = requests.get(ELASTIC_HOST+ELASTIC_LOG_INDEX)
    result_content = json.loads(result.content)[ELASTIC_LOG_INDEX]['mappings']['doc']['properties']
    if verbose:
        print('\n'*2,'**__ELASTIC_LOG_INDEX_INFO__**', '\n')
        print('\n'*2,'**__METHODS__**', '\n')
        pprint(dir(result))
        print('\n'*2,'**__KEYS__**', '\n')
        print(result_content.keys())
        print('\n'*2, '**__CONTENT__**', '\n')
        pprint(result_content)
        print('\n'*2)
    return result_content

def show_existent_tables(verbose=False):
    result = requests.get(ELASTIC_HOST+ELASTIC_FORMAT_TXT, json=ELASTIC_JSON_TABLES_QUERY)
    result_content = [x.split('|')[0].strip() for x in result.text.split('\n')\
                      if 'BASE TABLE' in x]
    if verbose:
        print('\n'*2, f'**__TABLES_INSIDE_ELASTICSEARCH__**', '\n')
        pprint(result_content)
    return result_content

def show_tables_columns(verbose=False):
    for table in DEFAULT_TABLES_LIST:
        temp_json = {"query": f"SHOW COLUMNS FROM \"{table}\""}
        result = requests.get(ELASTIC_HOST+ELASTIC_FORMAT_TXT,
                              json=temp_json)
        if verbose:
            print('\n'*2, f'**__COLUMS_FOR_{table.upper()}__**', '\n')
            pprint(result.text)
    return result.text



if __name__ == '__main__':
    print('-'*100)
    show_index_properties(True)
    print('-'*100)
    show_existent_tables(True)
    print('-'*100)
    show_tables_columns(True)
    
