-- This stops the swarm
-- docker swarm leave --force

-- This starts the swarm
-- docker swarm init

-- These are docker funcs:
-- docker container ls / kill
-- docker images ls / rm
-- docker ps

-- This builds the docker
-- docker build C:\Users\Gerardo.Feliz\Documents\djangotest\src --tag test


  CREATE TABLE attachments (
    filename VARCHAR(255),
    content_type VARCHAR(255),
    data BINARY(4294967295),
    created_at DATETIME,
    updated_at DATETIME
    )


  CREATE TABLE banner_videos (
    campaign_id BIGINT(4),
    interval_start DATETIME,
    interval_end DATETIME,
    total_basket_value DECIMAL(15, 6),
    total_budget DECIMAL(15, 6),
    vast_video_width BIGINT(4),
    vast_video_height BIGINT(4),
    bid_ecpm DECIMAL(15, 6),
    vast_video_linerarity BIGINT(4),
    vast_video_duration BIGINT(4),
    vast_video_type TEXT(65535),
    vast_video_outgoing_file TEXT(16777215),
    bids BIGINT(4),
    clicks BIGINT(4),
    pixels BIGINT(4),
    wins BIGINT(4),
    total_cost DECIMAL(15, 6),
    daily_cost DECIMAL(15, 6),
    daily_budget DECIMAL(15, 6),
    frequency_spec TEXT(65535),
    frequency_expire BIGINT(4),
    frequency_count BIGINT(4),
    created_at DATETIME,
    updated_at DATETIME,
    hourly_budget DECIMAL(15, 6),
    name VARCHAR(255),
    target_id BIGINT(4),
    hourly_cost DECIMAL(15, 6),
    bitrate BIGINT(4),
    mime_type VARCHAR(255),
    deals VARCHAR(255),
    width_range VARCHAR(255),
    height_range VARCHAR(255),
    width_height_list VARCHAR(255)
    )


  CREATE TABLE banner_videos_rtb_standards (
    banner_video_id BIGINT(4),
    rtb_standard_id BIGINT(4)
    )


--  add_index "banner_videos_rtb_standards", ["banner_video_id"], name: "index_banner_videos_rtb_standards_on_banner_video_id", using: :btree
--  add_index "banner_videos_rtb_standards", ["rtb_standard_id"], name: "index_banner_videos_rtb_standards_on_rtb_standard_id", using: :btree

  CREATE TABLE banners (
    campaign_id BIGINT(4),
    interval_start DATETIME,
    interval_end DATETIME,
    total_basket_value DECIMAL(15, 6),
    width BIGINT(4),
    height BIGINT(4),
    bid_ecpm DECIMAL(15, 6),
    total_cost DECIMAL(15, 6),
    contenttype VARCHAR(1024),
    iurl VARCHAR(1024),
    htmltemplate TEXT(16777215),
    bids BIGINT(4),
    clicks BIGINT(4),
    pixels BIGINT(4),
    wins BIGINT(4),
    daily_budget DECIMAL(15, 6),
    hourly_budget DECIMAL(15, 6),
    daily_cost DECIMAL(15, 6),
    target_id BIGINT(4),
    created_at DATETIME,
    updated_at DATETIME,
    name VARCHAR(255),
    frequency_spec VARCHAR(255),
    frequency_expire BIGINT(4),
    frequency_count BIGINT(4),
    hourly_cost DECIMAL(15, 6),
    deals VARCHAR(255),
    width_range VARCHAR(255),
    height_range VARCHAR(255),
    width_height_list VARCHAR(255)
    )


  CREATE TABLE banners_rtb_standards (
    banner_id BIGINT(4),
    rtb_standard_id BIGINT(4)
    )


--  add_index "banners_rtb_standards", ["banner_id"], name: "index_banners_rtb_standards_on_banner_id", using: :btree
--  add_index "banners_rtb_standards", ["rtb_standard_id"], name: "index_banners_rtb_standards_on_rtb_standard_id", using: :btree

  CREATE TABLE campaigns (
    activate_time DATETIME,
    expire_time DATETIME,
    cost DECIMAL(15, 6),
    ad_domain VARCHAR(1024),
    clicks BIGINT(4),
    pixels BIGINT(4),
    wins BIGINT(4),
    bids BIGINT(4),
    name VARCHAR(1024),
    status VARCHAR(1024),
    conversion_type VARCHAR(1024),
    budget_limit_daily DECIMAL(15, 6),
    budget_limit_hourly DECIMAL(15, 6),
    total_budget DECIMAL(15, 6),
    bid DECIMAL(15, 6),
    shard TEXT(65535),
    forensiq TEXT(65535),
    daily_cost DECIMAL(15, 6),
    updated_at DATETIME,
    deleted_at DATETIME,
    created_at DATETIME,
    hourly_cost DECIMAL(15, 6),
    exchanges VARCHAR(255),
    regions VARCHAR(255),
    target_id BIGINT(4)
    )


  CREATE TABLE campaigns_rtb_standards (
    campaign_id BIGINT(4),
    rtb_standard_id BIGINT(4)
    )


--  add_index "campaigns_rtb_standards", ["campaign_id"], name: "index_campaigns_rtb_standards_on_campaign_id", using: :btree
--  add_index "campaigns_rtb_standards", ["rtb_standard_id"], name: "index_campaigns_rtb_standards_on_rtb_standard_id", using: :btree

  CREATE TABLE categories (
    name VARCHAR(1024),
    description VARCHAR(2048),
    updated_at DATETIME,
    created_at DATETIME
    )


  CREATE TABLE categories_documents (
    document_id BIGINT(4),
    category_id BIGINT(4)
    )


--  add_index "categories_documents", ["category_id"], name: "index_categories_documents_on_category_id", using: :btree
--  add_index "categories_documents", ["document_id"], name: "index_categories_documents_on_document_id", using: :btree

  CREATE TABLE countries (
    sort_order VARCHAR(255),
    common_name VARCHAR(255),
    formal_name VARCHAR(255),
    country_type VARCHAR(255),
    sub_type VARCHAR(255),
    sovereignty VARCHAR(255),
    capital VARCHAR(255),
    iso_4217_currency_code VARCHAR(255),
    iso_4217_currency_name VARCHAR(255),
    iso_3166_1_2_letter_code VARCHAR(255),
    iso_3166_1_3_letter_code VARCHAR(255),
    iso_3166_1_number VARCHAR(255),
    iana_country_code_tld VARCHAR(255),
    itu_t_telephone_code VARCHAR(255)
    )


  CREATE TABLE documents (
    name VARCHAR(1024),
    description VARCHAR(2048),
    doctype VARCHAR(1024),
    code TEXT(65535),
    updated_at DATETIME,
    created_at DATETIME
    )


  CREATE TABLE exchange_attributes (
    banner_id BIGINT(4),
    banner_video_id BIGINT(4),
    name VARCHAR(255),
    value VARCHAR(255),
    created_at DATETIME,
    updated_at DATETIME,
    exchange VARCHAR(255)
    )


--  add_index "exchange_attributes", ["banner_id"], name: "index_exchange_attributes_on_banner_id", using: :btree
--  add_index "exchange_attributes", ["banner_video_id"], name: "index_exchange_attributes_on_banner_video_id", using: :btree

  CREATE TABLE exchange_rtbspecs (
    rtbspecification VARCHAR(1024),
    operand_type VARCHAR(1024),
    operand_ordinal VARCHAR(1024),
    updated_at DATETIME,
    deleted_at DATETIME
    )


  CREATE TABLE exchange_smarty_ads_rtbspecs (
    rtbspecification VARCHAR(1024),
    operand_type VARCHAR(1024),
    operand_ordinal VARCHAR(1024),
    updated_at DATETIME,
    deleted_at DATETIME
    )


-- THE NAME OF group IS NOW iab_categories_group DUE TO COLUMN NAMES RESTRICTIONS

  CREATE TABLE iab_categories (
    group TEXT(65535),
    name TEXT(65535),
    iab_id TEXT(65535),
    is_group TINYINT(1),
    created_at DATETIME,
    updated_at DATETIME
    )


  CREATE TABLE lists (
    name VARCHAR(1024),
    description VARCHAR(4096),
    list_type VARCHAR(1024),
    filesize BIGINT(4),
    s3_url VARCHAR(4096),
    filepath VARCHAR(4096),
    filetype VARCHAR(4096),
    last_modified VARCHAR(1024),
    created_at DATETIME,
    updated_at DATETIME
    )


  CREATE TABLE report_commands (
    name VARCHAR(1024),
    type VARCHAR(1024),
    campaign_id BIGINT(4),
    description VARCHAR(2048),
    command TEXT(65535),
    created_at DATETIME,
    updated_at DATETIME,
    banner_id BIGINT(4),
    banner_video_id BIGINT(4)
    )


  CREATE TABLE rtb_standards (
    rtbspecification VARCHAR(1024),
    operator VARCHAR(1024),
    operand VARCHAR(1024),
    operand_type VARCHAR(16),
    operand_ordinal VARCHAR(16),
    rtb_required TINYINT(1),
    name VARCHAR(255),
    description VARCHAR(255),
    created_at DATETIME,
    updated_at DATETIME,
    operand_list_id BIGINT(4)
    )


  CREATE TABLE stats_rtb (
    campaign_id BIGINT(4),
    stats_date DATETIME,
    bids BIGINT(4),
    wins BIGINT(4),
    clicks BIGINT(4),
    pixels BIGINT(4),
    win_price DECIMAL(15, 6),
    bid_price DECIMAL(15, 6)
    )


  CREATE TABLE targets (
    activate_time DATETIME,
    expire_time DATETIME,
    list_of_domains TEXT(16777215),
    domain_targetting VARCHAR(50),
    geo_latitude FLOAT(53),
    geo_longitude FLOAT(53),
    geo_range FLOAT(53),
    country TEXT(65535),
    geo_region TEXT(65535),
    carrier TEXT(65535),
    os TEXT(65535),
    make TEXT(65535),
    model TEXT(65535),
    devicetype TEXT(65535),
    IAB_category TEXT(65535),
    IAB_category_blklist TEXT(65535),
    created_at DATETIME,
    updated_at DATETIME,
    name VARCHAR(255),
    domains_list_id BIGINT(4)
    )

  CREATE TABLE users (
    name VARCHAR(255),
    email VARCHAR(255),
    password_digest VARCHAR(255),
    admin TINYINT(1),
    created_at DATETIME,
    updated_at DATETIME
    )

