echo DELETE CONTAINERS AND IMAGES IF THEY PERSIST
docker system prune -a -f
docker volume prune -f
docker swarm leave -f
docker swarm init
docker network create --driver overlay --attachable rtb4free_network
docker-compose rm -f
docker-compose down
docker-compose build --no-cache
docker-compose up --force-recreate
timeout /t 30