from rest_framework import serializers
from .models import Banners


class BannersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banners
        #fields = ("name", "campaign_id", "wins", "hourly_cost", "total_cost")
        fields = ("id", "campaign_id", "interval_start", "interval_end", "total_basket_value", "width", "height",
                  "bid_ecpm", "total_cost", "contenttype", "iurl", "htmltemplate", "bids", "clicks", "pixels",
                  "wins", "daily_budget", "hourly_budget", "daily_cost", "target_id", "created_at", "updated_at",
                  "name", "frequency_spec", "frequency_expire", "frequency_count", "hourly_cost", "deals",
                  "width_range", "height_range", "width_height_list",)