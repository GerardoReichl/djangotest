from django.test import TestCase

# Create your tests here.
from datetime import datetime
from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import Banners
from .serializers import BannersSerializer

class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_banner(campaign_id=2, interval_start=datetime.now(), interval_end='',
    				  total_basket_value='', width='', height='', bid_ecpm='', total_cost='',
    				  contenttype='', iurl='', htmltemplate='', bids='', clicks='', pixels='',
    				  wins='', daily_budget='', hourly_budget='', daily_cost='', target_id='',
    				  created_at=datetime.now(), updated_at=datetime.now(), name='',
    				  frequency_spec='', frequency_expire='', frequency_count='', hourly_cost='',
    				  deals='', width_range='', height_range='', width_height_list=''):
        if name != "" and total_cost != "":
            Banners.objects.create(name=name, total_cost=email)

    def setUp(self):

        # Add test data
        self.create_banner(name="like glue", total_cost=100)
        self.create_banner(name="simple song", total_cost=200)
        self.create_banner(name="love is wicked", total_cost=300)
        self.create_banner(name="jam rock", total_cost=400)


class GetAllBannersTest(BaseViewTest):

    def test_get_all_banners(self):
        """
        This test ensures that all banners added in the setUp method
        exist when we make a GET request to the banners/ endpoint
        """
        # Hit the API endpoint
        response = self.client.get(
            reverse("banners-all", kwargs={"version": "v1"})
        )
        # Fetch the data from db
        expected = Banners.objects.all()
        serialized = BannersSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


        