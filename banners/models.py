from django.db import models

# Create your models here.
class Banners(models.Model):
    id = models.IntegerField(blank=True, null=False, primary_key=True)
    campaign_id = models.IntegerField(blank=True, null=True)
    interval_start = models.DateTimeField(blank=True, null=True)
    interval_end = models.DateTimeField(blank=True, null=True)
    total_basket_value = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    bid_ecpm = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    total_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    contenttype = models.CharField(max_length=1024, blank=True, null=True)
    iurl = models.CharField(max_length=1024, blank=True, null=True)
    htmltemplate = models.TextField(blank=True, null=True)
    bids = models.IntegerField(blank=True, null=True)
    clicks = models.IntegerField(blank=True, null=True)
    pixels = models.IntegerField(blank=True, null=True)
    wins = models.IntegerField(blank=True, null=True)
    daily_budget = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    hourly_budget = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    daily_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    target_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    frequency_spec = models.CharField(max_length=255, blank=True, null=True)
    frequency_expire = models.IntegerField(blank=True, null=True)
    frequency_count = models.IntegerField(blank=True, null=True)
    hourly_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    deals = models.CharField(max_length=255, blank=True, null=True)
    width_range = models.CharField(max_length=255, blank=True, null=True)
    height_range = models.CharField(max_length=255, blank=True, null=True)
    width_height_list = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'banners'
