from django.db import models

# Create your models here.
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.


class Attachments(models.Model):
    filename = models.CharField(max_length=255, blank=True, null=True)
    content_type = models.CharField(max_length=255, blank=True, null=True)
    data = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'attachments'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class BannerVideos(models.Model):
    campaign_id = models.IntegerField(blank=True, null=True)
    interval_start = models.DateTimeField(blank=True, null=True)
    interval_end = models.DateTimeField(blank=True, null=True)
    total_basket_value = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    total_budget = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    vast_video_width = models.IntegerField(blank=True, null=True)
    vast_video_height = models.IntegerField(blank=True, null=True)
    bid_ecpm = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    vast_video_linerarity = models.IntegerField(blank=True, null=True)
    vast_video_duration = models.IntegerField(blank=True, null=True)
    vast_video_type = models.TextField(blank=True, null=True)
    vast_video_outgoing_file = models.TextField(blank=True, null=True)
    bids = models.IntegerField(blank=True, null=True)
    clicks = models.IntegerField(blank=True, null=True)
    pixels = models.IntegerField(blank=True, null=True)
    wins = models.IntegerField(blank=True, null=True)
    total_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    daily_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    daily_budget = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    frequency_spec = models.TextField(blank=True, null=True)
    frequency_expire = models.IntegerField(blank=True, null=True)
    frequency_count = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    hourly_budget = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    target_id = models.IntegerField(blank=True, null=True)
    hourly_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    bitrate = models.IntegerField(blank=True, null=True)
    mime_type = models.CharField(max_length=255, blank=True, null=True)
    deals = models.CharField(max_length=255, blank=True, null=True)
    width_range = models.CharField(max_length=255, blank=True, null=True)
    height_range = models.CharField(max_length=255, blank=True, null=True)
    width_height_list = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'banner_videos'


class BannerVideosRtbStandards(models.Model):
    banner_video_id = models.IntegerField(blank=True, null=True)
    rtb_standard_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'banner_videos_rtb_standards'


class Banners(models.Model):
    campaign_id = models.IntegerField(blank=True, null=True)
    interval_start = models.DateTimeField()
    interval_end = models.DateTimeField(blank=True, null=True)
    total_basket_value = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    bid_ecpm = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    total_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    contenttype = models.CharField(max_length=1024, blank=True, null=True)
    iurl = models.CharField(max_length=1024, blank=True, null=True)
    htmltemplate = models.TextField(blank=True, null=True)
    bids = models.IntegerField(blank=True, null=True)
    clicks = models.IntegerField(blank=True, null=True)
    pixels = models.IntegerField(blank=True, null=True)
    wins = models.IntegerField(blank=True, null=True)
    daily_budget = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    hourly_budget = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    daily_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    target_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    name = models.CharField(max_length=255, blank=True, null=True)
    frequency_spec = models.CharField(max_length=255, blank=True, null=True)
    frequency_expire = models.IntegerField(blank=True, null=True)
    frequency_count = models.IntegerField(blank=True, null=True)
    hourly_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    deals = models.CharField(max_length=255, blank=True, null=True)
    width_range = models.CharField(max_length=255, blank=True, null=True)
    height_range = models.CharField(max_length=255, blank=True, null=True)
    width_height_list = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'banners'


class BannersRtbStandards(models.Model):
    banner_id = models.IntegerField(blank=True, null=True)
    rtb_standard_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'banners_rtb_standards'


class Campaigns(models.Model):
    activate_time = models.DateTimeField(blank=True, null=True)
    expire_time = models.DateTimeField(blank=True, null=True)
    cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    ad_domain = models.CharField(max_length=1024, blank=True, null=True)
    clicks = models.IntegerField(blank=True, null=True)
    pixels = models.IntegerField(blank=True, null=True)
    wins = models.IntegerField(blank=True, null=True)
    bids = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=1024, blank=True, null=True)
    status = models.CharField(max_length=1024, blank=True, null=True)
    conversion_type = models.CharField(max_length=1024, blank=True, null=True)
    budget_limit_daily = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    budget_limit_hourly = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    total_budget = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    bid = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    shard = models.TextField(blank=True, null=True)
    forensiq = models.TextField(blank=True, null=True)
    daily_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    hourly_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    exchanges = models.CharField(max_length=255, blank=True, null=True)
    regions = models.CharField(max_length=255, blank=True, null=True)
    target_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'campaigns'


class CampaignsRtbStandards(models.Model):
    campaign_id = models.IntegerField(blank=True, null=True)
    rtb_standard_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'campaigns_rtb_standards'


class Categories(models.Model):
    name = models.CharField(max_length=1024, blank=True, null=True)
    description = models.CharField(max_length=2048, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'categories'


class CategoriesDocuments(models.Model):
    document_id = models.IntegerField(blank=True, null=True)
    category_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'categories_documents'


class Countries(models.Model):
    sort_order = models.CharField(max_length=255, blank=True, null=True)
    common_name = models.CharField(max_length=255, blank=True, null=True)
    formal_name = models.CharField(max_length=255, blank=True, null=True)
    country_type = models.CharField(max_length=255, blank=True, null=True)
    sub_type = models.CharField(max_length=255, blank=True, null=True)
    sovereignty = models.CharField(max_length=255, blank=True, null=True)
    capital = models.CharField(max_length=255, blank=True, null=True)
    iso_4217_currency_code = models.CharField(max_length=255, blank=True, null=True)
    iso_4217_currency_name = models.CharField(max_length=255, blank=True, null=True)
    itu_t_telephone_code = models.CharField(db_column='itu-t_telephone_code', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters.
    iso_3166_1_2_letter_code = models.CharField(db_column='iso_3166-1_2_letter_code', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters.
    iso_3166_1_3_letter_code = models.CharField(db_column='iso_3166-1_3_letter_code', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters.
    iso_3166_1_number = models.CharField(db_column='iso_3166-1_number', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters.
    iana_country_code_tld = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'countries'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Documents(models.Model):
    name = models.CharField(max_length=1024, blank=True, null=True)
    description = models.CharField(max_length=2048, blank=True, null=True)
    doctype = models.CharField(max_length=1024, blank=True, null=True)
    code = models.TextField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'documents'


class ExchangeAttributes(models.Model):
    banner_id = models.IntegerField(blank=True, null=True)
    banner_video_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    value = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    exchange = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'exchange_attributes'


class ExchangeRtbspecs(models.Model):
    rtbspecification = models.CharField(max_length=1024, blank=True, null=True)
    operand_type = models.CharField(max_length=1024, blank=True, null=True)
    operand_ordinal = models.CharField(max_length=1024, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'exchange_rtbspecs'


class ExchangeSmartyAdsRtbspecs(models.Model):
    rtbspecification = models.CharField(max_length=1024, blank=True, null=True)
    operand_type = models.CharField(max_length=1024, blank=True, null=True)
    operand_ordinal = models.CharField(max_length=1024, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'exchange_smarty_ads_rtbspecs'


class IabCategories(models.Model):
    group = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    iab_id = models.TextField(blank=True, null=True)
    is_group = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'iab_categories'


class Lists(models.Model):
    name = models.CharField(max_length=1024, blank=True, null=True)
    description = models.CharField(max_length=4096, blank=True, null=True)
    list_type = models.CharField(max_length=1024, blank=True, null=True)
    filesize = models.IntegerField(blank=True, null=True)
    s3_url = models.CharField(max_length=4096, blank=True, null=True)
    filepath = models.CharField(max_length=4096, blank=True, null=True)
    filetype = models.CharField(max_length=4096, blank=True, null=True)
    last_modified = models.CharField(max_length=1024, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'lists'


class ReportCommands(models.Model):
    name = models.CharField(max_length=1024, blank=True, null=True)
    type = models.CharField(max_length=1024, blank=True, null=True)
    campaign_id = models.IntegerField(blank=True, null=True)
    description = models.CharField(max_length=2048, blank=True, null=True)
    command = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    banner_id = models.IntegerField(blank=True, null=True)
    banner_video_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_commands'


class RtbStandards(models.Model):
    rtbspecification = models.CharField(max_length=1024, blank=True, null=True)
    operator = models.CharField(max_length=1024, blank=True, null=True)
    operand = models.CharField(max_length=1024, blank=True, null=True)
    operand_type = models.CharField(max_length=16, blank=True, null=True)
    operand_ordinal = models.CharField(max_length=16, blank=True, null=True)
    rtb_required = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    operand_list_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rtb_standards'


class SchemaMigrations(models.Model):
    version = models.CharField(unique=True, max_length=255)

    class Meta:
        managed = False
        db_table = 'schema_migrations'


class StatsRtb(models.Model):
    campaign_id = models.IntegerField(blank=True, null=True)
    stats_date = models.DateTimeField(blank=True, null=True)
    bids = models.IntegerField(blank=True, null=True)
    wins = models.IntegerField(blank=True, null=True)
    clicks = models.IntegerField(blank=True, null=True)
    pixels = models.IntegerField(blank=True, null=True)
    win_price = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    bid_price = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'stats_rtb'


class Targets(models.Model):
    activate_time = models.DateTimeField(blank=True, null=True)
    expire_time = models.DateTimeField(blank=True, null=True)
    list_of_domains = models.TextField(blank=True, null=True)
    domain_targetting = models.CharField(max_length=50, blank=True, null=True)
    geo_latitude = models.FloatField(blank=True, null=True)
    geo_longitude = models.FloatField(blank=True, null=True)
    geo_range = models.FloatField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)
    geo_region = models.TextField(blank=True, null=True)
    carrier = models.TextField(blank=True, null=True)
    os = models.TextField(blank=True, null=True)
    make = models.TextField(blank=True, null=True)
    model = models.TextField(blank=True, null=True)
    devicetype = models.TextField(blank=True, null=True)
    iab_category = models.TextField(db_column='IAB_category', blank=True, null=True)  # Field name made lowercase.
    iab_category_blklist = models.TextField(db_column='IAB_category_blklist', blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    domains_list_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'targets'


class Users(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    password_digest = models.CharField(max_length=255, blank=True, null=True)
    admin = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'users'
        
        
if '__name__' == '__main__':
    a = Users(name='jo', email='jo@basetis.com', password_digest='123456789A', admin=0)
    print(a)
