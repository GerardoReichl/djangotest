from django.contrib import admin

from . import db_models

# Register your models here.
admin.site.register(db_models.Attachments)
admin.site.register(db_models.AuthGroup)
admin.site.register(db_models.AuthGroupPermissions)
admin.site.register(db_models.AuthPermission)
admin.site.register(db_models.AuthUser)
admin.site.register(db_models.AuthUserGroups)
admin.site.register(db_models.AuthUserUserPermissions)
admin.site.register(db_models.BannerVideos)
admin.site.register(db_models.BannerVideosRtbStandards)
admin.site.register(db_models.Banners)
admin.site.register(db_models.BannersRtbStandards)
admin.site.register(db_models.Campaigns)
admin.site.register(db_models.CampaignsRtbStandards)
admin.site.register(db_models.Categories)
admin.site.register(db_models.CategoriesDocuments)
admin.site.register(db_models.Countries)
admin.site.register(db_models.DjangoAdminLog)
admin.site.register(db_models.DjangoContentType)
admin.site.register(db_models.DjangoMigrations)
admin.site.register(db_models.DjangoSession)
admin.site.register(db_models.Documents)
admin.site.register(db_models.ExchangeAttributes)
admin.site.register(db_models.Users)