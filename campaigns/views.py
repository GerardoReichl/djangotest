from django.shortcuts import render
from django.http import JsonResponse, Http404
from rest_framework import generics
from rest_framework.generics import GenericAPIView
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from .models import Campaigns as Model
from .serializers import CampaignsSerializer as ModelSerializer
from rest_framework import status


class ListView(GenericAPIView):
    """
    Provides a get method handler.
    """
    queryset = Model.objects.all()
    serializer_class = ModelSerializer

    def get(self, *args, **kwargs):
        item = Model.objects.all()
        serializer = ModelSerializer(item, many=True)
        return JsonResponse(serializer.data, safe=False)

    def post(self, request):
        serializer = ModelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


class DetailView(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        try:
            return Model.objects.get(pk=pk)
        except Model.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        item = self.get_object(pk)
        serializer = ModelSerializer(item)
        return JsonResponse(serializer.data)

    def put(self, request, pk, format=None):
        item = self.get_object(pk)
        serializer = ModelSerializer(item, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        item = self.get_object(pk)
        item.delete()
        return JsonResponse(status=status.HTTP_204_NO_CONTENT)