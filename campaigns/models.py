from django.db import models

# Create your models here.
class Campaigns(models.Model):
    id = models.IntegerField(blank=True, null=False, primary_key=True)
    activate_time = models.DateTimeField(blank=True, null=True)
    expire_time = models.DateTimeField(blank=True, null=True)
    cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    ad_domain = models.CharField(max_length=1024, blank=True, null=True)
    clicks = models.IntegerField(blank=True, null=True)
    pixels = models.IntegerField(blank=True, null=True)
    wins = models.IntegerField(blank=True, null=True)
    bids = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=1024, blank=True, null=True)
    status = models.CharField(max_length=1024, blank=True, null=True)
    conversion_type = models.CharField(max_length=1024, blank=True, null=True)
    budget_limit_daily = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    budget_limit_hourly = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    total_budget = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    bid = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    shard = models.TextField(blank=True, null=True)
    forensiq = models.TextField(blank=True, null=True)
    daily_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    hourly_cost = models.DecimalField(max_digits=15, decimal_places=6, blank=True, null=True)
    exchanges = models.CharField(max_length=255, blank=True, null=True)
    regions = models.CharField(max_length=255, blank=True, null=True)
    target_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'campaigns'