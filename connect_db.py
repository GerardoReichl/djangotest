import pymysql
from pprint import pprint

# query_0 = 'CREATE TABLE test_table (column1 VARCHAR(255), column2 VARCHAR(255), column3 VARCHAR(255));'

# query_1 = 'INSERT INTO test_table (column1, column2, column3) VALUES ("1","2","3")'

# query_2 = 'SELECT * from test_table;'

# query = 'SELECT table_name FROM information_schema.tables;'

# query = 'SELECT * FROM users;'

# query = 'SELECT * FROM campaigns;'

query = 'SHOW COLUMNS FROM users;'

def import_classes():
    try:
        import polls.models
    except ImportError as err:
        print('Cannot find polls.models for the import.')

def prepare_django():
    import django
    django.setup()

def parse_sql(filename):
    data = open(filename, 'r').readlines()
    stmts = []
    DELIMITER = ';'
    stmt = ''

    for lineno, line in enumerate(data):
        if not line.strip():
            continue

        if line.startswith('--'):
            continue

        if 'DELIMITER' in line:
            DELIMITER = line.split()[1]
            continue

        if (DELIMITER not in line):
            stmt += line.replace(DELIMITER, ';')
            continue

        if stmt:
            stmt += line
            stmts.append(stmt.strip())
            stmt = ''
        else:
            stmts.append(line.strip())
    return stmts


def create_tables():
    conn = pymysql.connect(host='db', port=3306, user='ben', passwd='test', db='rtb4free')
    cur = conn.cursor()
    cur.execute(query)
    # cur.execute(query_0)
    # cur.execute(query_1)
    # cur.execute(query_2)
    # stmts = parse_sql('all_tables_creation.sql')
    # for stmt in stmts:
    #     try:
    #         print(stmt)
    #         cur.execute(stmt)
    #     except Exception as e:
    #         print(f'Error with statement {stmt} - \n {e}')

    result = cur.fetchall()
    pprint(result)

    cur.execute('commit;')

    cur.close()
    conn.close()


if __name__ == '__main__':
    create_tables()
    
